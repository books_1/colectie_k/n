# N

## Content

```
./N. D. Cocea:
N. D. Cocea - Vinul de viata lunga 1.0 '{Literatura}.docx

./N. K. Jemisin:
N. K. Jemisin - Cele o suta de mii de regate 1.0 '{SF}.docx
N. K. Jemisin - Pamantul Sfaramat - V1 Al cincilea anotimp 1.0 '{SF}.docx
N. K. Jemisin - Pamantul Sfaramat - V2 Poarta obeliscului 1.0 '{SF}.docx
N. K. Jemisin - Pamantul Sfaramat - V3 Cerul de piatra 1.0 '{SF}.docx
N. K. Jemisin - Vise Intunecate - V1 Luna ucigasa 1.0 '{SF}.docx
N. K. Jemisin - Vise Intunecate - V2 Soarele umbrit 1.0 '{SF}.docx

./N. Lee Wood:
N. Lee Wood - Copiii lui Faraday 1.0 '{SF}.docx
N. Lee Wood - In cautarea lui Mahdi 1.0 '{SF}.docx

./N. N. Nagaev:
N. N. Nagaev - Destinul condamnatilor V2 0.99 '{SF}.docx

./N. Paduraru:
N. Paduraru - Vanatoarea 1.0 '{ClubulTemerarilor}.docx

./N. Popov:
N. Popov - Petru cel Mare - Intaiul revolutionar 0.9 '{Istorie}.docx

./N. Radulescu Lemnaru:
N. Radulescu Lemnaru - Povestea caporalului Filip 0.6 '{IstoricaRo}.docx

./N. S. Leskov:
N. S. Leskov - Vulturul alb 0.9 '{Diverse}.docx

./N. Steindhardt:
N. Steindhardt - Jurnalul fericirii 0.8 '{Filozofie}.docx

./Nadejda Mcguines:
Nadejda Mcguines - Astrologia dupa fazele lunii 0.99 '{Spiritualitate}.docx

./Nadine Cressay:
Nadine Cressay - La Micul Paradis 0.99 '{Romance}.docx

./Nae Cojocaru:
Nae Cojocaru - Filmul unei existente 0.9 '{Razboi}.docx

./Nae Ionescu:
Nae Ionescu - Curs de filosofie a religiei 0.99 '{Filozofie}.docx

./Naghib Mahfuz:
Naghib Mahfuz - Akhenaton, cel ce salasluieste in adevar 0.9 '{AventuraIstorica}.docx
Naghib Mahfuz - Baietii de pe strada noastra 1.0 '{Literatura}.docx
Naghib Mahfuz - Miramar 0.9 '{Literatura}.docx
Naghib Mahfuz - Palavrageala pe Nil 1.0 '{Literatura}.docx
Naghib Mahfuz - Rhadopis din Nubia 1.0 '{AventuraIstorica}.docx
Naghib Mahfuz - Trilogia Cairului - V1 O plimbare prin palat 1.0 '{Literatura}.docx

./Nancy Farmer:
Nancy Farmer - Casa scorpionului 1.0 '{Tineret}.docx
Nancy Farmer - Marea trolilor 1.0 '{Tineret}.docx

./Nancy John:
Nancy John - Un barbat nemilos 0.99 '{Dragoste}.docx

./Nancy Kress:
Nancy Kress - Arta razboiului 0.99 '{SF}.docx
Nancy Kress - Foc incrucisat 2.0 '{SF}.docx
Nancy Kress - Inertia 1.0 '{SF}.docx
Nancy Kress - Muntele la Mahomed 0.99 '{SF}.docx
Nancy Kress - Nanotehnologia cucereste Clifford Falls 0.99 '{SF}.docx
Nancy Kress - O incalcare a drepturilor de autor 0.9 '{SF}.docx
Nancy Kress - Probabilistic - V1 Luna probabilistica 1.0 '{SF}.docx
Nancy Kress - Probabilistic - V2 Soarele probabilistic 1.0 '{SF}.docx
Nancy Kress - Probabilistic - V3 Spatiul probabilistic 1.0 '{SF}.docx
Nancy Kress - Un zeu din umbra 0.9 '{SF}.docx

./Nancy Thayer:
Nancy Thayer - Spiritul ratacitor 0.6 '{Dragoste}.docx
Nancy Thayer - Un zambet ca o zi de primavara 0.99 '{Romance}.docx

./Nancy Woodruff:
Nancy Woodruff - Iubire vinovata 1.0 '{Romance}.docx

./Nany Herbert:
Nany Herbert - Vieti duble 0.2 '{Dragoste}.docx
Nany Herbert - Viitorul ne zambeste 0.9 '{Romance}.docx

./Naomi Klein:
Naomi Klein - Doctrina socului 1.0 '{DezvoltarePersonala}.docx

./Naomi Novik:
Naomi Novik - Aleasa dragonului 1.0 '{AventuraIstorica}.docx
Naomi Novik - Argintul preschimbat 1.0 '{AventuraIstorica}.docx
Naomi Novik - Temeraire - V1 Dragonul maiestatii sale 1.0 '{AventuraIstorica}.docx
Naomi Novik - Temeraire - V2 Tronul de jad 1.0 '{AventuraIstorica}.docx
Naomi Novik - Temeraire - V3 Razboiul pulberii negre 1.0 '{AventuraIstorica}.docx
Naomi Novik - Temeraire - V4 Imperiul de fildes 1.0 '{AventuraIstorica}.docx
Naomi Novik - Temeraire - V5 Victoria vulturilor 1.0 '{AventuraIstorica}.docx

./Naomi Ozaniec:
Naomi Ozaniec - Elemente de ceakra 1.0 '{DezvoltarePersonala}.docx

./Natale Benazzi:
Natale Benazzi - Cartea neagra a inchizitiei 1.0 '{Istorie}.docx

./Natalie Stone:
Natalie Stone - Parteneri deplini 0.99 '{Dragoste}.docx

./Nathanael West:
Nathanael West - Miss Lonely Hearts. Ziua lacustei 0.7 '{Diverse}.docx

./Nathan Filer:
Nathan Filer - Socul caderii 0.9 '{Literatura}.docx

./Nathan Hill:
Nathan Hill - Nix 1.0 '{Literatura}.docx

./Nathaniel Hawthorne:
Nathaniel Hawthorne - Litera stacojie 1.0 '{Literatura}.docx

./Natsuo Kirino:
Natsuo Kirino - Insula Tokyo 1.0 '{Literatura}.docx

./Neagoe Basarab:
Neagoe Basarab - Invataturile lui Neagoe Basarab catre fiul sau Teodosie 1.0 '{ClasicRo}.docx

./Neagoe Manole:
Neagoe Manole - Testamentul 0.9 '{IstoricaRo}.docx

./Neagu Djuvara:
Neagu Djuvara - Amintiri din pribegie 0.9 '{Interzise}.docx
Neagu Djuvara - Dincolo de timp 1.0 '{Interzise}.docx
Neagu Djuvara - O scurta istorie a romanilor povestita celor tineri 1.0 '{Istorie}.docx

./Neagu Radulescu:
Neagu Radulescu - Un balon radea in poarta 1.9 '{Sport}.docx

./Neal Asher:
Neal Asher - Agentul Cormac 1.0 '{SF}.docx
Neal Asher - Cowl 1.0 '{SF}.docx

./Neale Donald Walsch:
Neale Donald Walsch - Bazele Spiritualitatii 1.0 '{Spiritualitate}.docx
Neale Donald Walsch - Cateva fragmente din cartea Conversatii cu Dumnezeu pentru adolescenti si parinti 1.0 '{Spiritualitate}.docx
Neale Donald Walsch - Cei ce aduc lumina 1.0 '{Spiritualitate}.docx
Neale Donald Walsch - Ce vrea Dumnezeu 1.0 '{Spiritualitate}.docx
Neale Donald Walsch - Comuniune cu Dumnezeu 0.99 '{Spiritualitate}.docx
Neale Donald Walsch - Conversatii cu Dumnezeu V1 0.99 '{Spiritualitate}.docx
Neale Donald Walsch - Conversatii cu Dumnezeu V2 0.99 '{Spiritualitate}.docx
Neale Donald Walsch - Conversatii cu Dumnezeu V3 0.99 '{Spiritualitate}.docx
Neale Donald Walsch - Dumnezeul de maine 0.2 '{Spiritualitate}.docx
Neale Donald Walsch - Dumnezeu si minunile din viata noastra 0.9 '{Spiritualitate}.docx
Neale Donald Walsch - Mesaje prin Neale Donald Walsch 0.2 '{Spiritualitate}.docx
Neale Donald Walsch - Micul suflet si Soarele 0.2 '{Spiritualitate}.docx
Neale Donald Walsch - Momente de gratie 0.4 '{Spiritualitate}.docx
Neale Donald Walsch - Prietenie cu Dumnezeu V1 0.5 '{Spiritualitate}.docx
Neale Donald Walsch - Prietenie cu Dumnezeu V2 0.6 '{Spiritualitate}.docx

./Neal Shusterman:
Neal Shusterman - Arc of a Scythe - V1 Secera 1.0 '{Literatura}.docx

./Neal Stephenson:
Neal Stephenson - Era de diamant 1.0 '{SF}.docx
Neal Stephenson - Mataraie 1.0 '{SF}.docx
Neal Stephenson - Seveneves - V1 Sapte Eve 1.0 '{SF}.docx
Neal Stephenson - Snow Crash 2.0 '{SF}.docx

./Neculai Popa:
Neculai Popa - Coborarea in iad 0.9 '{Comunism}.docx

./Neil Anderson:
Neil Anderson - Temeiul tuturor lucrurilor 0.8 '{Spiritualitate}.docx

./Neil Gaiman:
Neil Gaiman - Anansi boys 2.0 '{SF}.docx
Neil Gaiman - Cartea cimitirului 3.0 '{SF}.docx
Neil Gaiman - Mitologia nordica 1.0 '{SF}.docx
Neil Gaiman - Oceanul de la capatul aleii 1.0 '{SF}.docx
Neil Gaiman - Pulbere de stele 1.0 '{SF}.docx
Neil Gaiman - Zei americani 3.0 '{SF}.docx

./Neil LaBute:
Neil LaBute - All apologies 0.99 '{Necenzurat}.docx
Neil LaBute - Autobahn 0.99 '{Teatru}.docx
Neil LaBute - Bash 0.9 '{Diverse}.docx
Neil LaBute - Forma lucrurilor 0.99 '{Teatru}.docx
Neil LaBute - Funny 0.99 '{Teatru}.docx
Neil LaBute - Long division 0.99 '{Teatru}.docx
Neil LaBute - Road trip 0.99 '{Teatru}.docx

./Neil Strauss:
Neil Strauss - Jocul seductiei 1.0 '{Erotic}.docx

./Nelly:
Nelly - Secretul castelului Strinberg 0.99 '{Dragoste}.docx
Nelly - Tara portii de aur 0.99 '{Dragoste}.docx

./Nelson DeMille:
Nelson DeMille - Caderea noptii 4.0 '{Thriller}.docx

./Nelson Demille:
Nelson Demille - Asasinul singuratic 2.0 '{Thriller}.docx
Nelson Demille - Coasta de aur 1.0 '{Thriller}.docx
Nelson Demille - Cuvant de onoare 1.0 '{Thriller}.docx
Nelson Demille - Fabrica de spioni 2.5 '{Thriller}.docx
Nelson Demille - Fiica generalului 1.0 '{Thriller}.docx
Nelson Demille - Focul salbatic 1.0 '{Thriller}.docx
Nelson Demille - Intoarcerea la coasta de aur 1.0 '{Thriller}.docx
Nelson Demille - Jocul leului 1.0 '{Thriller}.docx
Nelson Demille - La apa Babilonului 1.0 '{Thriller}.docx
Nelson Demille - Leul 1.0 '{Thriller}.docx
Nelson Demille - Misiune ingrata 2.0 '{Thriller}.docx
Nelson Demille - Odiseea lui Talbot 1.0 '{Thriller}.docx
Nelson Demille - Plum Island 2.0 '{Thriller}.docx
Nelson Demille - Spencerville 1.0 '{Thriller}.docx

./Nelu Ionescu:
Nelu Ionescu - Neincredere in Foisor 1.0 '{Teatru}.docx

./Nelu Tironeac:
Nelu Tironeac - Drumul regasit 0.5 '{Literatura}.docx

./Nely Cab:
Nely Cab - Creatura 1.0 '{SF}.docx

./Nerea Riesco:
Nerea Riesco - Ars magica 1.0 '{AventuraIstorica}.docx

./Nestor Urechia:
Nestor Urechia - Bucegi 0.9 '{Natura}.docx

./Netta Muskett:
Netta Muskett - Casa de la stavilar 0.2 '{Romance}.docx
Netta Muskett - Casatorie surpriza 0.99 '{Dragoste}.docx
Netta Muskett - Da-mi viata inapoi 0.99 '{Dragoste}.docx
Netta Muskett - Dragoste rasplatita 0.99 '{Dragoste}.docx
Netta Muskett - Se sparg norii 0.9 '{Romance}.docx

./Neville Scott:
Neville Scott - Orasul ultimei sanse 2.0 '{Western}.docx

./Nevil Shute:
Nevil Shute - Ultimul tarm 3.0 '{SF}.docx
Nevil Shute - Un oras ca Alice 1.0 '{SF}.docx

./Nguyen Du:
Nguyen Du - Povestea arborelui de Capoc 0.9 '{BasmesiPovesti}.docx

./Niccolo Ammaniti:
Niccolo Ammaniti - Cum vrea Dumnezeu 0.9 '{Diverse}.docx

./Niccolo Machiavelli:
Niccolo Machiavelli - Arta razboiului 1.0 '{Filozofie}.docx
Niccolo Machiavelli - Principele 2.0 '{Istorie}.docx

./Nicholas Boothman:
Nicholas Boothman - Cum sa construiesti relatii personale in 90 de secunde 1.0 '{Psihologie}.docx

./Nicholas Evans:
Nicholas Evans - Tamaduitorul 0.9 '{Thriller}.docx
Nicholas Evans - Vieti incercate 1.0 '{Thriller}.docx

./Nicholas Monsarrat:
Nicholas Monsarrat - Marea cruda V1 & V2 0.9 '{ActiuneRazboi}.docx

./Nicholas Sparks:
Nicholas Sparks - Accidentul 1.0 '{Romance}.docx
Nicholas Sparks - Cel mai de pret cadou 1.0 '{Romance}.docx
Nicholas Sparks - Cel mai lung drum 1.0 '{Romance}.docx
Nicholas Sparks - Draga John 2.0 '{Romance}.docx
Nicholas Sparks - Dragoste la prima vedere 1.0 '{Romance}.docx
Nicholas Sparks - Jurnalul unei iubiri 1.0 '{Romance}.docx
Nicholas Sparks - Mesaj de departe 1.0 '{Romance}.docx
Nicholas Sparks - Miracolul 1.0 '{Romance}.docx
Nicholas Sparks - Nopti in Rodanthe 1.0 '{Romance}.docx
Nicholas Sparks - Nunta 1.0 '{Romance}.docx
Nicholas Sparks - O plimbare de neuitat 1.0 '{Romance}.docx
Nicholas Sparks - Pas in doi 1.0 '{Romance}.docx
Nicholas Sparks - Priveste-ma! 1.0 '{Romance}.docx
Nicholas Sparks - Refugiul 1.0 '{Romance}.docx
Nicholas Sparks - Talismanul norocos 1.0 '{Romance}.docx
Nicholas Sparks - Ultimul cantec 1.0 '{Romance}.docx

./Nick Drake:
Nick Drake - Nefertiti. Cartea mortilor 1.0 '{AventuraIstorica}.docx

./Nick Hornby:
Nick Hornby - Cum sa fii bun 0.9 '{Literatura}.docx
Nick Hornby - High fidelity 1.0 '{Literatura}.docx
Nick Hornby - Slam 0.99 '{Literatura}.docx
Nick Hornby - Totul despre baieti 0.99 '{Literatura}.docx
Nick Hornby - Turnul sinucigasilor 0.99 '{Literatura}.docx

./Nick Mason:
Nick Mason - Inside out. O istorie personala a Pink Floyd 1.0 '{Biografie}.docx

./Nick Pope:
Nick Pope - Ceruri deschise, minti incuiate 0.9 '{MistersiStiinta}.docx

./Nicodim Aghioratul:
Nicodim Aghioratul - Cele mai frumoase rugaciuni ale ortodoxiei 1.0 '{Religie}.docx

./Nicodim Mandita:
Nicodim Mandita - Calea sufletelor in vesnicie - Vamile vazduhului 1.0 '{Religie}.docx
Nicodim Mandita - Indreptar pentru spovedanie 0.9 '{Religie}.docx
Nicodim Mandita - Inmormantarea si parastasele cuvenite 1.0 '{Religie}.docx

./Nico Di Savoya:
Nico Di Savoya - Primul om pe Venus 0.9 '{SF}.docx

./Nicola Andrews:
Nicola Andrews - Idila in munti 1.0 '{Romance}.docx
Nicola Andrews - Pana la pierderea respiratiei 0.9 '{Romance}.docx

./Nicolae Balcescu:
Nicolae Balcescu - Romanii supt Mihai-Voievod Viteazul 0.9 '{ClasicRo}.docx

./Nicolae Bara:
Nicolae Bara - Oglinda sufletul 0.99 '{Versuri}.docx

./Nicolae Boboc:
Nicolae Boboc - Colinda pacurarului 0.8 '{Diverse}.docx

./Nicolae Breban:
Nicolae Breban - Animale bolnave 1.0 '{Literatura}.docx
Nicolae Breban - Bunavestire 1.0 '{Literatura}.docx

./Nicolae Ciobanu:
Nicolae Ciobanu - Supusul Satanei - V1 In lumea intunericului 1.0 '{Diverse}.docx
Nicolae Ciobanu - Supusul Satanei - V2 Pe Taramul zorilor 1.0 '{Diverse}.docx

./Nicolae Ciolacu:
Nicolae Ciolacu - Haiducii Dobrogei 0.9 '{Politica}.docx

./Nicolae Coande:
Nicolae Coande - Galeria cu barbati 0.99 '{Diverse}.docx

./Nicolae Dabija:
Nicolae Dabija - Tema pentru acasa 2.0 '{Diverse}.docx

./Nicolae Filimon:
Nicolae Filimon - Ciocoii Vechi si Noi - V1 Ciocoii vechi 1.0 '{IstoricaRo}.docx
Nicolae Filimon - Omul de piatra 1.0 '{IstoricaRo}.docx

./Nicolae Franculescu:
Nicolae Franculescu - Colorado - V1 Coltii sacalului 4.0 '{Western}.docx
Nicolae Franculescu - Colorado - V2 La sud de Rio Bravo 4.0 '{Western}.docx

./Nicolae Gane:
Nicolae Gane - Domnita Ruxandra 1.0 '{IstoricaRo}.docx
Nicolae Gane - Pacate marturisite 1.0 '{IstoricaRo}.docx

./Nicolae Ioana:
Nicolae Ioana - Pasagerul 1.0 '{Literatura}.docx

./Nicolae Ionescu:
Nicolae Ionescu - Azilul de orbi. Regina Elisabeta. Vatra luminoasa 0.8 '{Istorie}.docx

./Nicolae Ionescu Dunararu:
Nicolae Ionescu Dunararu - Eu, Tase si politia 1.0 '{AventuraTineret}.docx

./Nicolae Iorga:
Nicolae Iorga - Adevarul asupra trecutului si prezentului Basarabiei 1.0 '{Istorie}.docx
Nicolae Iorga - Albania si Romania 1.0 '{Istorie}.docx
Nicolae Iorga - America si romanii din America 0.6 '{Istorie}.docx
Nicolae Iorga - Continuitatea spiritului romanesc in Basarabia 1.0 '{Istorie}.docx
Nicolae Iorga - Dezvoltarea imperialismului contemporan 1.0 '{Istorie}.docx
Nicolae Iorga - Istoria lui Stefan cel Mare 1.0 '{Istorie}.docx
Nicolae Iorga - Istoria romanilor prin calatori 1.0 '{Istorie}.docx
Nicolae Iorga - O viata de om asa cum a fost 1.0 '{Istorie}.docx
Nicolae Iorga - Pagini despre Basarabia de astazi 1.0 '{Istorie}.docx
Nicolae Iorga - Privelisti din tara 1.0 '{Istorie}.docx
Nicolae Iorga - Romania mama a unitatii nationale 1.0 '{Istorie}.docx

./Nicolae Macovei:
Nicolae Macovei - Un om cu trenci gri si ilustratii abstracte 0.9 '{Diverse}.docx

./Nicolae Margeanu:
Nicolae Margeanu - Alte aventuri ale capitanului Vigu 1.0 '{Politista}.docx
Nicolae Margeanu - Romanul care ucide 1.0 '{Politista}.docx

./Nicolae Miulescu:
Nicolae Miulescu - Dacia - tara zeilor 1.0 '{Dacia}.docx

./Nicolae Mladin:
Nicolae Mladin - Doctrina despre viata a profesorului Nicolae Paulescu 0.99 '{Biografie}.docx

./Nicolae Motoc:
Nicolae Motoc - Golful salbatic 1.0 '{Dragoste}.docx

./Nicolae Olexiuc:
Nicolae Olexiuc - 1800 retete culinare practice 0.9 '{Alimentatie}.docx

./Nicolae Paulescu:
Nicolae Paulescu - Spitalul, Coranul, Talmudul, Kahalul si francmasoneria 0.8 '{Sanatate}.docx

./Nicolae Paul Mihail:
Nicolae Paul Mihail - Damen vals 1.0 '{SF}.docx
Nicolae Paul Mihail - Femeia cibernetica 1.0 '{SF}.docx
Nicolae Paul Mihail - Potirul sfantului Pancratiu 1.0 '{SF}.docx

./Nicolae Rosca:
Nicolae Rosca - Corneliu Zelea Codreanu 0.9 '{Politica}.docx
Nicolae Rosca - Geneza miscarilor nationaliste in Europa 0.9 '{Politica}.docx
Nicolae Rosca - Inchisoarea Suceava 0.9 '{Politica}.docx
Nicolae Rosca - Martirologul legionar 0.9 '{Politica}.docx
Nicolae Rosca - Miscarea legionara si problema muncitoreasca 0.8 '{Politica}.docx
Nicolae Rosca - Voi fascistii sunteti teroristii 0.9 '{Politica}.docx

./Nicolae Sarbu:
Nicolae Sarbu - Neputinta de a inchide cercul 0.99 '{Versuri}.docx

./Nicolae Steinhardt:
Nicolae Steinhardt - Despre fidelitate 0.99 '{Filozofie}.docx

./Nicolae Strambeanu:
Nicolae Strambeanu - Scrisoarea pe care nu o vei citi niciodata 0.8 '{Necenzurata}.docx
Nicolae Strambeanu - Turnul de apa 0.7 '{Diverse}.docx

./Nicolae Tautu:
Nicolae Tautu - Cinci impuscaturi in cer 2.0 '{Politista}.docx
Nicolae Tautu - Enigmatica Solveig 1.0 '{Politista}.docx
Nicolae Tautu - Misiunile capitanului Dan 1.0 '{Politista}.docx
Nicolae Tautu - Secretul documentului X 1.0 '{Politista}.docx

./Nicolae Tic:
Nicolae Tic - Grindina 0.7 '{Diverse}.docx
Nicolae Tic - Orasul cu o mie de blesteme 0.8 '{Diverse}.docx

./Nicolae Velimirovici:
Nicolae Velimirovici - Predici 1-10 0.9 '{Religie}.docx
Nicolae Velimirovici - Predici 11-20 0.9 '{Religie}.docx
Nicolae Velimirovici - Predici 21-28 0.9 '{Religie}.docx

./Nicola Griffith:
Nicola Griffith - Amonit 1.0 '{SF}.docx
Nicola Griffith - Raul linistit 2.0 '{SF}.docx

./Nicolai Cernasevski:
Nicolai Cernasevski - Ce-i de facut 1.0 '{Literatura}.docx

./Nicolai Lilin:
Nicolai Lilin - Educatie siberiana 1.0 '{ActiuneRazboi}.docx

./Nicolas Freeling:
Nicolas Freeling - Regele unei tari ploioase 0.8 '{Politista}.docx

./Nicola Upson:
Nicola Upson - Un expert in crima 1.0 '{Politista}.docx

./Nicola Yoon:
Nicola Yoon - Absolut tot 1.0 '{SF}.docx
Nicola Yoon - Si Soarele e o stea 1.0 '{SF}.docx

./Nicole Baart:
Nicole Baart - Departe 1.0 '{Literatura}.docx

./Nicole Brooks:
Nicole Brooks - Adiere de vara 0.99 '{Romance}.docx
Nicole Brooks - Secretul doctorului 0.99 '{Dragoste}.docx

./Nicole Kornher Stace:
Nicole Kornher Stace - Arhivista WASP 1.0 '{Supranatural}.docx

./Nicoleta Olimpia Gherghel:
Nicoleta Olimpia Gherghel - Ritual alb 1.0 '{Literatura}.docx

./Nicole Valery Grossu:
Nicole Valery Grossu - Binecuvantata fii, inchisoare 0.9 '{Politica}.docx

./Nic Pizzolatto:
Nic Pizzolatto - Galveston 1.0 '{Politista}.docx

./Nigel Warburton:
Nigel Warburton - Cum sa gandim corect si eficient 0.99 '{DezvoltarePersonala}.docx

./Nikita Lalwani:
Nikita Lalwani - Harul 0.99 '{Literatura}.docx

./Niklas Dag:
Niklas Dag - Bellman Noir - V1 1793. In umbra mortii 1.0 '{AventuraIstorica}.docx

./Nikolai Berdiaev:
Nikolai Berdiaev - Filosofia lui Dostoievski 0.8 '{Filozofie}.docx

./Nikolai Nosov:
Nikolai Nosov - Aventurile lui Habarnam 0.99 '{Tineret}.docx

./Nikolai Osipov:
Nikolai Osipov - Din lumea vrajita a plantelor 1.0 '{Copii}.docx

./Nikolai Pavlov:
Nikolai Pavlov - Ancora ramane in adanc 1.0 '{Politista}.docx

./Nikolai Vasilievici Gogol:
Nikolai Vasilievici Gogol - Caleasca 1.0 '{ClasicSt}.docx
Nikolai Vasilievici Gogol - Insemnarile unui nebun 1.0 '{ClasicSt}.docx
Nikolai Vasilievici Gogol - Mantaua 1.0 '{ClasicSt}.docx
Nikolai Vasilievici Gogol - Meditatii la Dumnezeiasca liturghie 1.0 '{ClasicSt}.docx
Nikolai Vasilievici Gogol - Nasul 1.0 '{ClasicSt}.docx
Nikolai Vasilievici Gogol - Nevski prospekt 1.0 '{ClasicSt}.docx
Nikolai Vasilievici Gogol - Opere V1 1.0 '{ClasicSt}.docx
Nikolai Vasilievici Gogol - Portretul 1.0 '{ClasicSt}.docx
Nikolai Vasilievici Gogol - Suflete moarte 1.0 '{ClasicSt}.docx
Nikolai Vasilievici Gogol - Taras Bulba 1.0 '{ClasicSt}.docx
Nikolai Vasilievici Gogol - Vii 1.0 '{ClasicSt}.docx

./Nikos Kazantzakis:
Nikos Kazantzakis - Alexis Zorba 1.0 '{AventuraIstorica}.docx
Nikos Kazantzakis - Capitanul Mihalis 0.9 '{AventuraIstorica}.docx
Nikos Kazantzakis - Raport catre El Greco 1.0 '{AventuraIstorica}.docx

./Nina Berberova:
Nina Berberova - Acompaniatoarea 1.0 '{Literatura}.docx
Nina Berberova - Cartea fericirii 1.0 '{Literatura}.docx
Nina Berberova - Doamnele din Sankt Petersburg 1.0 '{Literatura}.docx
Nina Berberova - Invierea lui Mozart 1.0 '{Literatura}.docx
Nina Berberova - Romanul de capa si lacrimi 0.6 '{Literatura}.docx
Nina Berberova - Trestia revoltata 1.0 '{Literatura}.docx

./Nina Malkin:
Nina Malkin - Blestemul din Swoon 1.0 '{Supranatural}.docx

./Nina Stanculescu:
Nina Stanculescu - Templul scufundat 1.0 '{Tineret}.docx

./Nina Vasile:
Nina Vasile - Distanta din miezul noptii 0.9 '{Versuri}.docx

./Nis Petersen:
Nis Petersen - Strada sandalarilor 0.7 '{Literatura}.docx

./Noah Gordon:
Noah Gordon - Catalanul 1.0 '{AventuraIstorica}.docx
Noah Gordon - Diamantul Ierusalimului 1.0 '{AventuraIstorica}.docx
Noah Gordon - Saga Familiei Cole - V1 Doctorul 3.0 '{Literatura}.docx
Noah Gordon - Saga Familiei Cole - V2 Saman 1.0 '{Literatura}.docx
Noah Gordon - Saga Familiei Cole - V3 Casa de pe colina 1.0 '{Literatura}.docx
Noah Gordon - Ultimul evreu 1.0 '{AventuraIstorica}.docx

./Noah Hawley:
Noah Hawley - Inainte de cadere 1.0 '{Literatura}.docx

./Noel Calef:
Noel Calef - Ascensor spre esafod 1.0 '{Politista}.docx

./Noelle Berry Mccue:
Noelle Berry Mccue - Taramul indragostitilor 0.9 '{Dragoste}.docx
Noelle Berry Mccue - Un strain venit de departe 0.9 '{Dragoste}.docx
Noelle Berry Mccue - Zambetul Joannei 0.99 '{Dragoste}.docx

./Nona Gamel:
Nona Gamel - Un suras ca o raza de soare 0.9 '{Dragoste}.docx

./Nora Iuga:
Nora Iuga - Blogstory 0.9 '{Diverse}.docx
Nora Iuga - Sexagenara si tanarul 0.7 '{Diverse}.docx
Nora Iuga - Spitalul manechinelor 0.9 '{Diverse}.docx

./Nora Roberts:
Nora Roberts - A fost odata o stea 1.0 '{Romance}.docx
Nora Roberts - Cheia - V1 Cheia luminii 0.99 '{Romance}.docx
Nora Roberts - Cheia - V2 Cheia cunoasterii 0.99 '{Romance}.docx
Nora Roberts - Cheia - V3 Cheia curajului 0.99 '{Romance}.docx
Nora Roberts - Comori tainuite 1.0 '{Romance}.docx
Nora Roberts - Dezvaluire socanta 1.0 '{Romance}.docx
Nora Roberts - Doamna furata 0.99 '{Romance}.docx
Nora Roberts - Dream - V1 Puterea visului 1.0 '{Romance}.docx
Nora Roberts - Gallaghers of Ardmore - V1 Diamantele vin din cer 1.0 '{Romance}.docx
Nora Roberts - Gallaghers of Ardmore - V3 Perlele marii 1.0 '{Romance}.docx
Nora Roberts - Hanul Amintirilor - V1 Treptele iubirii 1.0 '{Romance}.docx
Nora Roberts - Hanul Amintirilor - V2 Pasi spre fericire 1.0 '{Romance}.docx
Nora Roberts - Hanul Amintirilor - V3 Iubire peste timp 1.0 '{Romance}.docx
Nora Roberts - Imblanzitoarea de suflete 1.0 '{Romance}.docx
Nora Roberts - In the Garden - V1 Dalia albastra 1.0 '{Romance}.docx
Nora Roberts - In the Garden - V2 Trandafirul negru 1.0 '{Romance}.docx
Nora Roberts - In the Garden - V3 Crinul rosu 1.0 '{Romance}.docx
Nora Roberts - Iubiri fierbinti si tradari 0.99 '{Romance}.docx
Nora Roberts - Jurnalele unei prostituate de lux 1.0 '{Romance}.docx
Nora Roberts - La noapte si intotdeauna 1.0 '{Romance}.docx
Nora Roberts - Legaturi primejdioase 1.0 '{Romance}.docx
Nora Roberts - Mai imbatator decat vinul 1.0 '{Romance}.docx
Nora Roberts - O'hurleys - V1 Sacrificiu 1.0 '{Romance}.docx
Nora Roberts - Pacate sacre 1.0 '{Romance}.docx
Nora Roberts - Pastreaza visul 1.0 '{Romance}.docx
Nora Roberts - Pentru un trandafir salbatic 1.0 '{Romance}.docx
Nora Roberts - Secrete publice 0.99 '{Romance}.docx
Nora Roberts - Sign of Seven - V1 Frati de cruce 1.0 '{Romance}.docx
Nora Roberts - Sign of Seven - V2 Ziua a saptea 0.3 '{Romance}.docx
Nora Roberts - Sign of Seven - V3 Stanca pagana 0.4 '{Romance}.docx
Nora Roberts - Trilogia Cercului - V1 Crucea lui Morrigan 1.0 '{Romance}.docx
Nora Roberts - Trilogia Cercului - V2 Dansul zeilor 1.0 '{Romance}.docx
Nora Roberts - Trilogia Cercului - V3 Valea tacerii 1.0 '{Romance}.docx
Nora Roberts - Triumful dragostei 1.5 '{Romance}.docx
Nora Roberts - Visul regasit 0.8 '{Romance}.docx
Nora Roberts - Visuri implinite 1.0 '{Romance}.docx

./Nora Vasilescu:
Nora Vasilescu - Carte de despartire 0.9 '{Versuri}.docx

./Norma Daniels:
Norma Daniels - Fantomele trecutului 0.9 '{Dragoste}.docx

./Norma Jeffrey:
Norma Jeffrey - Nodul strans al dragostei 0.99 '{Dragoste}.docx

./Norma Michelson:
Norma Michelson - Santaj 0.99 '{Dragoste}.docx

./Norman A. Fox:
Norman A. Fox - Fulgerul neintrerupt 1.0 '{Western}.docx

./Norman Mailer:
Norman Mailer - Cei morti si cei goi 0.9 '{ActiuneRazboi}.docx
Norman Mailer - Seri antice 1.0 '{Literatura}.docx

./Norman Spinrad:
Norman Spinrad - Agentul haosului 1.0 '{SF}.docx
Norman Spinrad - Alte Americi 1.0 '{SF}.docx
Norman Spinrad - Bug Jack Barron 1.0 '{SF}.docx
Norman Spinrad - Intre doua lumi 2.0 '{SF}.docx
Norman Spinrad - Masinaria Rock & Roll 1.0 '{SF}.docx
Norman Spinrad - Oameni in jungla 2.0 '{SF}.docx
Norman Spinrad - Solarienii 2.0 '{SF}.docx
Norman Spinrad - Visul de fier 1.0 '{SF}.docx

./Norma Popper:
Norma Popper - O promisiune pe viata 0.99 '{Romance}.docx
Norma Popper - Profesoara de engleza 0.99 '{Dragoste}.docx

./Norma Rae:
Norma Rae - Micuta Jenny 0.99 '{Dragoste}.docx

./Norma Weston:
Norma Weston - Logodnicul altei femei 0.99 '{Dragoste}.docx

./Nothomb Amelie:
Nothomb Amelie - Acid sulfuric 0.99 '{Thriller}.docx

./Noul Excentric Club:
Noul Excentric Club - V01 Herbert Wolfner - Percy invinge trei campioni mondiali 2.0 '{Tineret}.docx
Noul Excentric Club - V03 Joe Weyermoor - Enigma din Paris 2.0 '{Tineret}.docx
Noul Excentric Club - V04 Martin Winfried - Iron King, calul minune 2.0 '{Tineret}.docx
Noul Excentric Club - V06 Herbert Wulfner - Misterul aerului 2.0 '{Tineret}.docx
Noul Excentric Club - V09 Otto Eicke - La intrecere cu Expresul de Nissa 1.0 '{Tineret}.docx
Noul Excentric Club - V26 Regele fotbalistilor 2.0 '{Tineret}.docx
Noul Excentric Club - V27 In trezorul bancii Angliei 2.0 '{Tineret}.docx
Noul Excentric Club - V30 Un pseudo Percy Stuart 2.0 '{Tineret}.docx
Noul Excentric Club - V31 Otrava familiei Borgia 1.0 '{Tineret}.docx
Noul Excentric Club - V75 Enigma din Madeira 2.0 '{Tineret}.docx
Noul Excentric Club - V83 In ghearele pasiunii 2.0 '{Tineret}.docx
Noul Excentric Club - V89 Banditul din Black Hills 2.0 '{Tineret}.docx
Noul Excentric Club - V115 Insula fericitilor 1.0 '{Tineret}.docx
Noul Excentric Club - V120 Spaima Congo-ului 2.0 '{Tineret}.docx
Noul Excentric Club - V121 Secretul catacombelor 2.0 '{Tineret}.docx
Noul Excentric Club - V122 In tara skipetarilor 2.0 '{Tineret}.docx
Noul Excentric Club - V132 Secretul stalpului 1.0 '{Tineret}.docx
Noul Excentric Club - V139 Moartea lui Vanderstyfts 1.0 '{Tineret}.docx
Noul Excentric Club - V152 Bancnota No. 333444 1.0 '{Tineret}.docx
Noul Excentric Club - V153 Clubul cocosatilor 1.0 '{Tineret}.docx
Noul Excentric Club - V154 Moartea indiana 1.0 '{Tineret}.docx
Noul Excentric Club - V157 Mortul din automobil 1.0 '{Tineret}.docx
Noul Excentric Club - V159 Un pariu pe viata si pe moarte 1.0 '{Tineret}.docx
Noul Excentric Club - V160 Tabachera de aur [Lipsa pag. 15-18] 1.0 '{Tineret}.docx
Noul Excentric Club - V169 Hotii de perle 1.0 '{Tineret}.docx
Noul Excentric Club - V170 Mostenitorul de milioane disparut 1.0 '{Tineret}.docx
Noul Excentric Club - V171 Ochiul lui Chavial 1.0 '{Tineret}.docx
Noul Excentric Club - V172 Joe Weyermoor - Spre Tibet 1.0 '{Tineret}.docx
Noul Excentric Club - V173 Walter Gerusheim - Fantana de sange din Khanpur 1.0 '{Tineret}.docx
Noul Excentric Club - V174 Walter Gerusheim - Vaduva Raia-ului 1.0 '{Tineret}.docx
Noul Excentric Club - V175 Walter Gerusheim - Logodnica tigrului 1.0 '{Tineret}.docx
Noul Excentric Club - V187 Wiliam Horst - Milionar peste noapte 1.0 '{Tineret}.docx
Noul Excentric Club - V218 Walter Gerusheim - O scrisoare misterioasa 1.0 '{Tineret}.docx

./Nuria Massot:
Nuria Massot - Umbra templierului 5.0 '{AventuraIstorica}.docx

./Nyiszli Miklos:
Nyiszli Miklos - Am fost medic la Auschwitz 1.0 '{Politica}.docx
```

